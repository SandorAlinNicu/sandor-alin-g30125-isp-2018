package g30125.Sandor.Alin.l2.e3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class primenumbers {

    public static void main(String[] args) throws IOException {

    	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("A=");
		int A = Integer.parseInt(br.readLine());
		System.out.print("B=");
		int B = Integer.parseInt(br.readLine());
        while (A < B) {
            boolean flag = false;
            for(int i = 2; i <= A/2; ++i) {
                if(A % i == 0) {
                    flag = true;
                    break;
                }
            }

            if (!flag)
                System.out.print(A + " ");

            ++A;
        }
    }
}