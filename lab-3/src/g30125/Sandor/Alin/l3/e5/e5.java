package g30125.Sandor.Alin.l3.e5;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Thing;
import becker.robots.Wall;

public class e5 {
	public static void main(String[] args)
	{
		City prague = new City();
		 Robot karel = new Robot(prague, 1, 2, Direction.SOUTH);
		 Thing parcel = new Thing(prague, 2, 2);
		 Wall blockAve0 = new Wall(prague, 1, 1, Direction.WEST);
		 Wall blockAve1 = new Wall(prague, 2, 1, Direction.WEST);
		 Wall blockAve2 = new Wall(prague, 1, 1, Direction.NORTH);
		 Wall blockAve3 = new Wall(prague, 1, 2, Direction.NORTH);
		 Wall blockAve4 = new Wall(prague, 1, 2, Direction.EAST);
		 Wall blockAve5 = new Wall(prague, 1, 2, Direction.SOUTH);
		 Wall blockAve7 = new Wall(prague, 2, 1, Direction.SOUTH);
		 karel.turnLeft();
		 karel.turnLeft();
		 karel.turnLeft();
		 karel.move();
		 karel.turnLeft();
		 karel.move();
		 karel.turnLeft();
		 karel.move();
		 karel.pickThing();
		 karel.turnLeft();
		 karel.turnLeft();
		 karel.move();
		 karel.turnLeft();
		 karel.turnLeft();
		 karel.turnLeft();
		 karel.move();
		 karel.turnLeft();
		 karel.turnLeft();
		 karel.turnLeft();
		 karel.move();
		 karel.turnLeft();
		 karel.turnLeft();
		 karel.turnLeft();
		 karel.putThing();
	}
}
